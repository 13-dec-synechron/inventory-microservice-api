package com.classpath.inventorymicroservice.processor;

import com.classpath.inventorymicroservice.model.EventType;
import com.classpath.inventorymicroservice.model.Order;
import com.classpath.inventorymicroservice.model.OrderEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.cloud.stream.messaging.Sink.INPUT;

@Component
@RequiredArgsConstructor
@Slf4j
public class OrderProcessor {

    private final Source source;

    @StreamListener(INPUT)
    public void processOrder(OrderEvent orderEvent){
        log.info("Processed the order :: {} ", orderEvent);
        log.info("Publishing the event ");
        this.source
                .output()
                .send(MessageBuilder.withPayload(OrderEvent.builder().order(orderEvent.getOrder()).eventType(EventType.ORDER_ACCEPTED).build()).build());
    }
}